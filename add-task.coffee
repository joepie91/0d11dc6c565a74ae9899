TaskRunner = require("./lib/task-runner")
runner = new TaskRunner(app: app, db: shelf, config: config, thumbnailPath: path.join(__dirname, 'thumbnails'))
runner.addTask "thumbnail", require("./tasks/thumbnail")
runner.run()

# [...]

req.taskRunner.do "thumbnail", id: slugID